package com.ulyanova.nstu.sixthterm;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Panel contains nested rectangles {@link Rectangle}
 */
public class RectangleCanvasPanel extends JPanel {

	private Random random = new Random();
	private int verticalPadding = -20;
	private static int MAX_COLOR = 256;

	public RectangleCanvasPanel(int padding) {
		if (padding > 0 && padding < Toolkit.getDefaultToolkit().getScreenSize().height) {

			this.verticalPadding = padding;
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D graphics2d = (Graphics2D) g;
		Dimension screenDimension = this.getSize();
		int coordinateX = 0;
		int coordinateY = 0;
		Rectangle rect = new Rectangle(coordinateX, coordinateY, screenDimension.width, screenDimension.height);
		graphics2d.fill(rect);
		double ratio = screenDimension.width * 1.0 / screenDimension.height;
		System.out.println(ratio);
		boolean tooSmall = false;
		while (!tooSmall) {
			graphics2d.setPaint(getRandomColor());

			coordinateX = coordinateX + (int) (verticalPadding * ratio);
			coordinateY = coordinateY + verticalPadding;
			double height = rect.getHeight() - 2 * verticalPadding;
			double width = height * ratio;

			rect = new Rectangle(coordinateX, coordinateY, (int) width, (int) height);
			//System.out.println("w/h: " + rect.getWidth()*1.0/ rect.getHeight());
			graphics2d.fill(rect);
			if (rect.getWidth() < verticalPadding) {
				tooSmall = true;
			}
		}

	}

	private Color getRandomColor() {
		return new Color(random.nextInt(MAX_COLOR), random.nextInt(MAX_COLOR), random.nextInt(MAX_COLOR));
	}
}
