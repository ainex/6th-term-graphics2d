package com.ulyanova.nstu.sixthterm;
import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Frame contains {@link RectangleCanvasPanel} where all the rectangles are to be drawn
 * Contain all components
 */
public class CanvasFrame extends JFrame {

	private JPanel rectangleCanvasPanel;

	public CanvasFrame(int padding)  {
		this.rectangleCanvasPanel = new RectangleCanvasPanel(padding);
	}

	public void drawRectangles(){
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setUndecorated(true);
		this.setVisible(true);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.getContentPane().add(rectangleCanvasPanel);
		this.addKeyListener(new EscKeyPressListener());

	}

	class EscKeyPressListener extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent ke) {
			if(ke.getKeyCode() == KeyEvent.VK_ESCAPE){
				CanvasFrame.this.dispose();
			}
		}
	}

}
