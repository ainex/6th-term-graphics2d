package com.ulyanova.nstu.sixthterm;

/**
 * Main application class.
 * This application allows to fill a full screen with random colored nested rectangles.
 * The padding value can be set by user.
 *
 * Creates {@link AppFrame} with all components
 */

public class App {

	public static void main(String[] args) {
		AppFrame frame = new AppFrame();
		frame.run();
	}
}
