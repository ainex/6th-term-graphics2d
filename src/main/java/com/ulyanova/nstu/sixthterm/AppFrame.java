package com.ulyanova.nstu.sixthterm;
import javax.swing.*;
import java.awt.*;

/**
 * Frame for the Rectangle Application
 * Contains {@link AppPanel} panel with UI components
 */
public class AppFrame extends JFrame {

	private final int width = 300;
	private final int height = 200;
	private final String title = "Rectangles filler";


	private JPanel appPanel;


	public AppFrame() {
		this.appPanel = new AppPanel();
	}

	public void run() {
		this.setSize(new Dimension(width, height));
		this.setTitle(title);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.getContentPane().add(appPanel);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}


}
