package com.ulyanova.nstu.sixthterm;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class AppPanel extends JPanel {
	public static final int COMPONENTS_HEIGHT = 20;
	public static final int COMPONENTS_WIDTH = 50;
	private Button button;
	private JSpinner spinner;
	private final String buttonLabel = "Draw Rectangles";
	private final String labelText = "Press ESC to exit the full screen view";

	public AppPanel() {
		Box verticalBox = Box.createVerticalBox();
		Box horizontalBox = Box.createHorizontalBox();

		horizontalBox.add(Box.createVerticalStrut(3 * COMPONENTS_HEIGHT));
		horizontalBox.add(createSpinner());
		horizontalBox.add(Box.createHorizontalStrut(20));
		horizontalBox.add(createButton());
		verticalBox.add(horizontalBox);
		verticalBox.add(createLabel());
		this.add(verticalBox);
	}

	private Button createButton() {
		button = new Button(buttonLabel);
		button.addActionListener(new DrawButtonListener());
		button.setMaximumSize(new Dimension(4 * COMPONENTS_WIDTH, COMPONENTS_HEIGHT));
		return button;
	}

	private JSpinner createSpinner() {
		int minPaddingValue = 5;
		int maxPaddingValue = 200;
		int startPaddingValue = 20;
		int stepValue = 10;
		SpinnerModel spinnerModel =
				new SpinnerNumberModel(startPaddingValue, minPaddingValue, maxPaddingValue, stepValue);
		spinner = new JSpinner(spinnerModel);
		spinner.setMaximumSize(new Dimension(COMPONENTS_WIDTH, COMPONENTS_HEIGHT));
		return spinner;
	}

	private Label createLabel() {
		Label label = new Label(labelText);
		return label;
	}


	class DrawButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int paddingValue = 20;
			try {
				paddingValue = (Integer) spinner.getValue();
			} catch (Exception exception) {
				//do nothing, default value will be used
			}
			CanvasFrame canvasFrame = new CanvasFrame(paddingValue);
			canvasFrame.drawRectangles();
		}
	}
}
